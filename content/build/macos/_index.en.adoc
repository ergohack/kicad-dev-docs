---
title: macOS
weight: 12
summary: Instructions for macOS using cmake and clang
tags: ["macos"]
---

:toc:

== Building KiCad on macOS

Building on macOS requires care, as specific versions of dependencies are required in order for all
KiCad features to compile and work properly. To simplify this process, we strongly recommend that
all macOS users use our https://gitlab.com/kicad/packaging/kicad-mac-builder[kicad-mac-builder]
utility, which contains the toolchain that produces the official macOS release builds of KiCad.

This utility can be used to set up a development environment for KiCad even if you just wish to
build the sources without creating a full release package (DMG containing the KiCad applications 
and libraries).

Building without `kicad-mac-builder` is possible but is not supported by the KiCad development
team. If you wish to pursue this route, please use the latest state of the kicad-mac-builder
repository for guidance as to which packages to install and how to configure and build them.

NOTE: KiCad requires a custom fork of wxWidgets on macOS to work properly. This fork is hosted at
      https://gitlab.com/kicad/code/wxWidgets[the KiCad GitLab].  Currently, the
      `kicad/macos-wx-3.1` branch should be used to build the master branch of KiCad.  If you use
      `kicad-mac-builder` to set up your build environment, the correct version of wxWidgets will
      be downloaded and built for you automatically.

=== Setting up development environment using kicad-mac-builder

This guide assumes you already have the appropriate Apple C++ development toolchain (Xcode) for
your macOS release installed.  See Apple's documentation for details on how to install Xcode or the
Xcode command-line tools.  `kicad-mac-builder` will not install this for you.

To obtain `kicad-mac-builder` and set up the KiCad build dependencies, run the following:

[source,sh]
```
cd <your preferred working directory>
git clone https://gitlab.com/kicad/packaging/kicad-mac-builder.git
cd kicad-mac-builder
./ci/src/bootstrap.sh
./build.py --target setup-kicad-dependencies
```

NOTE: The first command, `boostrap.sh`, will install Homebrew if it is not already installed, and
      install all the KiCad dependencies that are available as Homebrew packages.  You may wish to
      manually install the required dependencies instead of running this script.  If so, simply
      inspect the script to see the current list of required Homebrew packages to install.

At the end of the `kicad-mac-builder` build process, `build.py` will print a list of suggested
CMake arguments to the terminal.  **Copy this output to a safe place: you will need to refer to it
when you set up your KiCad build.**

Now you are ready to build KiCad itself.  Run the following to obtain and build KiCad:

[source,sh]
```
cd <your preferred working directory>
git clone https://gitlab.com/kicad/code/kicad.git
cd kicad
mkdir -p build/release
mkdir build/debug               # Optional for debug build.
cd build/release
cmake <arguments from kicad-mac-builder> ../..
make
make install
```

Paste in the arguments you saved from earlier after the `cmake` command.  Note that you may want to
inspect and modify some of the arguments before using them:

By default, `kicad-mac-builder` will set the `CMAKE_INSTALL_PREFIX` to `./build/kicad-dest` inside
your `kicad-mac-builder` directory.  Edit the arguments provided by `kicad-mac-builder` if you want
to install KiCad to a different location.

By default, `kicad-mac-builder` will set `CMAKE_BUILD_TYPE` to `RelWithDebInfo` to create an 
optimized release build with debugging symbols.  Change this to `Debug` to create an unoptimized
build that can be debugged using `lldb`.

NOTE: We recommend using Ninja instead of `make` for faster builds.  To do so, run
      `brew install ninja` and then add `-G Ninja` to your `cmake` arguments.
